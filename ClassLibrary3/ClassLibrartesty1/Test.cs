﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary3;
using NUnit.Framework;

namespace ClassLibrartesty1
{
    [TestFixture]
    public class Test
    {
        SqlConnector sqlConnector = new SqlConnector("Andrii", "131718");

        [TestCase(4)]
        public void CheckAmountUserFromTokyo(int expResult)
        {
            sqlConnector.ConnectToCatalog("HWSQLOrdersAndPersons");
            DataTable result = sqlConnector.Execute("SELECT * FROM Persons WHERE City = 'Tokyo'");
            Assert.AreEqual(expResult, result.Rows.Count);
        }
        [TestCase]
        public void СheckAllAmountCustomerAgeMore18()
        {
            bool flag = false;
            sqlConnector.ConnectToCatalog("HWSQLOrdersAndPersons");
            DataTable result = sqlConnector.Execute("SELECT*FROM Persons");
            for (int i = 0; i < result.Rows.Count; i++)
            {
                if (int.Parse(result.Rows[i].ItemArray[3].ToString()) < 18)
                {
                    flag = true;
                    break;
                }
                else
                {
                    flag = false;
                }
            }
            Assert.AreEqual(false, flag);
        }
        [TestCase]
        public void CheckOrderWithOrderSumNull()
        {
            bool flag = false;
            sqlConnector.ConnectToCatalog("HWSQLOrdersAndPersons");
            DataTable result = sqlConnector.Execute("SELECT * FROM Orders");
            for (int i = 0; i < result.Rows.Count; i++)
            {
                if (result.Rows[i].ItemArray[2].ToString().Length == 0)
                {
                    flag = true;
                    break;
                }
                else
                {
                    flag = false;
                }
            }
            Assert.AreEqual(false, flag);
        }
        [TestCase(51)]
        public void CheckThatWeHave51PersonsInDataBase(int expResult)
        {
            sqlConnector.ConnectToCatalog("HWSQLOrdersAndPersons");
            DataTable result = sqlConnector.Execute("SELECT * FROM Persons");
            Assert.AreEqual(expResult, result.Rows.Count);
        }

        [Test]
        public void CheckThatWeHavePersonsInDataBase()
        {
            sqlConnector.ConnectToCatalog("HWSQLOrdersAndPersons");
            DataTable result = sqlConnector.Execute("SELECT * FROM Persons");
            bool flag = false;
            
                if (result.Rows.Count > 0)
                {
                    flag = true;
                  
                }
                else
                {
                    flag = false;
                }
            
            Assert.AreEqual(true, flag);

        }
        [Test]
        public void CheckThatWeHaveOrdersInDataBase()
        {
            sqlConnector.ConnectToCatalog("HWSQLOrdersAndPersons");
            DataTable result = sqlConnector.Execute("SELECT * FROM Orders");
            bool flag = false;

            if (result.Rows.Count > 0)
            {
                flag = true;

            }
            else
            {
                flag = false;
            }
            Assert.AreEqual(true, flag);
        }
        [Test]
        public void CheckThatOrdersWasForPersonsWithRegistration()
        {
            bool flag = false;
            int count = 0;
            sqlConnector.ConnectToCatalog("HWSQLOrdersAndPersons");
            DataTable resultOrders = sqlConnector.Execute("SELECT * FROM Orders");
            DataTable resultPersons = sqlConnector.Execute("SELECT * FROM Persons");
            for (int i = 0; i < resultPersons.Rows.Count; i++)
            {
                for(int j = 0; j < resultOrders.Rows.Count; j++)
                {
                    if (resultOrders.Rows[j].ItemArray[0].ToString() == resultPersons.Rows[i].ItemArray[0].ToString())
                    {
                        count++;
                     
                    }
                    else
                    {
                        continue;
                    }
                }
                
            }
            if(count == resultOrders.Rows.Count)
            {
                flag = true;
            }
            Assert.AreEqual(true, flag);
        }
        [Test]
        public void CheckCreateNewPersonsIntoTable()
        {

            sqlConnector.ConnectToCatalog("HWSQLOrdersAndPersons");
            DataTable startTable = sqlConnector.Execute("SELECT*FROM Persons");
            sqlConnector.AddCommand($"INSERT INTO Persons (ID, FirstName, LastName, Age, City) VALUES({startTable.Rows.Count + 2 }, 'ohnTest', 'WickTest', 99, 'San Andreas');");
            DataTable result = sqlConnector.Execute("SELECT*FROM Persons");
            Assert.AreEqual((startTable.Rows.Count + 1), result.Rows.Count);

        }
        [Test]
        public void CheckDeleteNewPersonsIntoTable()
        {

            sqlConnector.ConnectToCatalog("HWSQLOrdersAndPersons");
            DataTable startTable = sqlConnector.Execute("SELECT*FROM Persons");
            sqlConnector.AddCommand($"DELETE FROM Persons WHERE ID = {startTable.Rows.Count+1}");
            DataTable result = sqlConnector.Execute("SELECT*FROM Persons");
            Assert.AreEqual((startTable.Rows.Count - 1), result.Rows.Count);

        }
    }
}