﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using System.Data.SqlClient;
using System.Data;

namespace ClassLibrary3
{
    public class SqlConnector
    {
        private readonly SqlCredential _credential;
        private string _connectionString;

        public SqlConnector(string login, string password)
        {
            //convert password
            var credential = new SecureString();
            for (var i = 0; i < password.Length; i++)
                credential.InsertAt(i, password[i]);

            credential.MakeReadOnly();
            //save your Credential for request
            _credential = new SqlCredential(login, credential);
        }
        public void ConnectToCatalog(string catalogName)
        {
            _connectionString = "Data Source = (localdb)\\MSSQLLocalDB; " +
                $"Initial Catalog = {catalogName}; ";
        }
        public DataTable Execute(string sqlRequest)
        {
            var dataSet = new DataSet();
            using (var sqlConnection = new SqlConnection(_connectionString, _credential))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(sqlRequest, sqlConnection))
                {
                    sqlCommand.CommandText = sqlRequest;
                    var adapter = new SqlDataAdapter(sqlCommand);
                    adapter.Fill(dataSet);
                }
            }
            if (sqlRequest.StartsWith("SELECT") && dataSet.Tables[0].Rows[0] != null)
            {
                return dataSet.Tables[0];
            }
            else
            {
                throw new Exception("Not OK");
            }
        }
        public void AddCommand(string sqlRequest)
        {
            //request result
            var dataSet = new DataSet();

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString, _credential))
            {
                sqlConnection.Open();
                //create sql request
                using (SqlCommand sqlCommand = new SqlCommand(sqlRequest, sqlConnection))
                {
                    sqlCommand.CommandText = sqlRequest;
                    //execute sql request
                    var adapter = new SqlDataAdapter(sqlCommand);
                    //Save result
                    adapter.Fill(dataSet);
                }
                
            }
            //return dataSet.Tables[1];


        }




        }
    }
